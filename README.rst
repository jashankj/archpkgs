========================================================================
			 jashankj.space/archpkg
========================================================================

::
  [jashankj]
  Server = https://jashankj.space/archpkg/

this repository, its packages,
and commits to the infrastructure repository
are gpg signed using my current key,
available in <https://jashankj.space/pubkey.asc>.
the infrastructure and packages are in git,
at <https://gitlab.com/jashankj/archpkgs>
