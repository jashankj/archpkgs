#!/usr/bin/env zsh
. "${ADMIN_DIR-admin}/CONFIG"

set -o extendedglob
set -o nullglob

title_progress() {
	local n="$1"
	local N="$2"
	local pkg="$3"
	case $TERM in
		xterm*) printf '\e]0;[%d/%d] %s\a' "$n" "$N" "$pkg" ;;
		*) ;;
	esac
}


cd "${TREE}"

pkgs=($(ls-packages | xargs sortpkg))

interactive=1

build1() {
	local pkg="$1"
	echo ": nice -n20 BUILD1 $pkg"
	nice -n20 BUILD1 "$pkg"
}

# Ask whether to rebuild a package.
query_rebuild() {
	local pkg="$1"

	# TODO: cache the result.
	rbp="$( if rebuild-p "$pkg"; then echo -n "yes"; else echo -n "no"; fi )"
	echo -n "$pkg ([y]es/[n]o/force, default ${rbp}: "
	if [ $interactive -eq 1 ] && [ "$rbp" = no ]
	then
		echo "[no]"
		continue
	fi

	read
	if \
		[[ "$REPLY" =~ '^[[:space:]]*[Nn][Oo]?[[:space:]]*$' ]]
	then
		continue
	elif \
		[[ "$REPLY" =~ '^[[:space:]]*[Yy][Ee]?[Ss]?[[:space:]]*$' ]] || \
		[[ "$REPLY" =~ '^[[:space:]]*[Ff][Oo][Rr][Cc][Ee][[:space:]]*$' ]] || \
		rebuild-p "$pkg"
	then
		build1 "$pkg"
	fi
}

n=0
N=${#pkgs}
for pkg in ${pkgs}
do
	(( n++ ))
	title_progress $n $N "$pkg"

	if [ $interactive != 0 ]
	then
		query_rebuild "$pkg"
	else
		rebuild-p "$pkg" &&
		build1 "$pkg"
	fi
done
